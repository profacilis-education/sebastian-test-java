package com.profacilis;

import java.io.Serializable;

public abstract class Animal implements Serializable, Movable {

	private Type type;
	private int numberOfLegs;
	private boolean hasTail;

	public Type getType() {
		return type;
	}

	public int getNumberOfLegs() {
		return numberOfLegs;
	}

	public boolean isHasTail() {
		return hasTail;
	}

	public void setType(Type type) {
		this.type = type;
	}

	public void setNumberOfLegs(int numberOfLegs) {
		this.numberOfLegs = numberOfLegs;
	}

	public void setHasTail(boolean hasTail) {
		this.hasTail = hasTail;
	}
}
