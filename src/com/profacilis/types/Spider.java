package com.profacilis.types;

import com.profacilis.Animal;
import com.profacilis.Type;

public class Spider extends Animal {

	public Spider() {
		super.setType(Type.SPIDER);
		super.setNumberOfLegs(8);
		super.setHasTail(false);
	}

	@Override
	public boolean canFly() {
		return false;
	}

	@Override
	public boolean canSwim() {
		return false;
	}

}
