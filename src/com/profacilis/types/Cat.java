package com.profacilis.types;

import com.profacilis.Animal;
import com.profacilis.Type;

public class Cat extends Animal {

	public Cat() {
		super.setType(Type.CAT);
		super.setNumberOfLegs(4);
		super.setHasTail(true);
	}

	@Override
	public boolean canFly() {
		return false;
	}

	@Override
	public boolean canSwim() {
		return false;
	}
}
