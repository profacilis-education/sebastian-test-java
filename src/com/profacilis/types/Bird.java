package com.profacilis.types;

import com.profacilis.Animal;
import com.profacilis.Type;

public class Bird extends Animal {

	public Bird() {
		super.setType(Type.BIRD);
		super.setNumberOfLegs(2);
		super.setHasTail(true);
	}

	@Override
	public boolean canFly() {
		return true;
	}

	@Override
	public boolean canSwim() {
		return false;
	}
}
