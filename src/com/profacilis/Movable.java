package com.profacilis;

public interface Movable {
	boolean canFly();
	boolean canSwim();
}
