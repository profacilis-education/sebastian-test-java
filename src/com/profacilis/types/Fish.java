package com.profacilis.types;

import com.profacilis.Animal;
import com.profacilis.Type;

public class Fish extends Animal {

	public Fish() {
		super.setType(Type.FISH);
		super.setNumberOfLegs(0);
		super.setHasTail(false);
	}

	@Override
	public boolean canFly() {
		return false;
	}

	@Override
	public boolean canSwim() {
		return true;
	}

}
