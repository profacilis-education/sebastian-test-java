package com.profacilis.types;

import com.profacilis.Animal;
import com.profacilis.Type;

public class Snake extends Animal {

	public Snake() {
		super.setNumberOfLegs(0);
		super.setType(Type.SNAKE);
		super.setHasTail(false);
	}

	@Override
	public boolean canFly() {
		return false;
	}

	@Override
	public boolean canSwim() {
		return true;
	}

}
