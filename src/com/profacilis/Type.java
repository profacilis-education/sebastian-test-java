package com.profacilis;

public enum Type {

	CAT("kot", "gray", true),
	BIRD("ptak", "white", true),
	SPIDER("pająk", "black", false),
	WASP("osa", "yellow", false),
	FISH("ryba", "blue", true),
	SNAKE("wąż", "green", false);

	private final String polish;
	private final String color;
	private final boolean nice;

	Type(String polish, String color, boolean nice) {
		this.polish = polish;
		this.color = color;
		this.nice = nice;
	}

	public String getPolish() {
		return polish;
	}

	public String getColor() {
		return color;
	}

	public boolean isNice() {
		return nice;
	}
}
