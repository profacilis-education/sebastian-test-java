package com.profacilis.types;

import com.profacilis.Animal;
import com.profacilis.Type;

public class Wasp extends Animal {

	public Wasp() {
		super.setType(Type.WASP);
		super.setNumberOfLegs(6);
		super.setHasTail(false);
	}

	@Override
	public boolean canFly() {
		return true;
	}

	@Override
	public boolean canSwim() {
		return false;
	}
}
