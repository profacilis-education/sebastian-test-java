package com.profacilis;

import com.profacilis.types.*;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
		int x = 10;

	    Scanner scanner = new Scanner(System.in);
	    while (scanner.hasNextLine()) {
		    String what = scanner.nextLine();
		    Type type;
		    if (what.contains("bye")) break;
		    try {
			    type = Type.valueOf(what.toUpperCase());
		    } catch (IllegalArgumentException e) {
		    	System.out.printf("Maybe you know such an animal - %s, I don't\n", what);
		    	continue;
		    }
		    Animal animal = getRightAnimal(type);
		    System.out.printf("This animal has %s legs and %s%n", animal.getNumberOfLegs(), animal.isHasTail() ? "has a tail" : "has no tail");
		    System.out.printf("In Polish: %s\n", animal.getType().getPolish());
		    System.out.printf("Its colour is %s\n", animal.getType().getColor());
		    System.out.printf("Can it fly?: %s\n", canFly(animal));
		    System.out.printf("Can it swim?: %s\n", canSwim(animal));
		    System.out.printf("Is it nice?: %s\n", animal.getType().isNice() ? "YES" : "NO");
	    }

    }

    private static String canFly(Animal animal) {
	    return animal.canFly() ? "YES" : "NO";
    }

	private static String canSwim(Animal animal) {
		return animal.canSwim() ? "YES" : "NO";
	}

    private static Animal getRightAnimal(Type type) {
    	Animal animal;
    	switch (type) {
		    case CAT -> animal = new Cat();
		    case BIRD -> animal = new Bird();
		    case SPIDER -> animal = new Spider();
		    case SNAKE -> animal = new Snake();
		    case WASP -> animal = new Wasp();
		    case FISH -> animal = new Fish();
		    default -> animal = null;
	    }
	    return animal;
    }
}
